cmake_minimum_required(VERSION 3.16)
project(similarcraft C)

set(SRC_DIR ./src)
set(INCLUDE_LIBSIMILAR ${CMAKE_CURRENT_LIST_DIR}/lib/libsimilar/include)
set(BIN_LIBSIMILAR ${CMAKE_CURRENT_LIST_DIR}/lib/libsimilar/build/libsimilar.a)
set(INCLUDE_LIBCNBT ${CMAKE_CURRENT_LIST_DIR}/lib/libcnbt/include)
set(BIN_LIBCNBT ${CMAKE_CURRENT_LIST_DIR}/lib/libcnbt/build/libcnbt.a)

file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS "${SRC_DIR}/*.h" "${SRC_DIR}/*.c")

add_compile_options(-g -O0)
add_executable(similarcraft ${SRC_FILES})
target_include_directories(similarcraft PRIVATE ${INCLUDE_LIBSIMILAR})
target_link_libraries(similarcraft ${BIN_LIBSIMILAR})
target_include_directories(similarcraft PRIVATE ${INCLUDE_LIBCNBT})
target_link_libraries(similarcraft ${BIN_LIBCNBT})
