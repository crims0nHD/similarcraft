#include "sponge_file.h"
#include "libcnbt.h"

#include "minecraft_logic.h"
#include "minecraft_structures.h"
#include <stdint.h>
#include <stdlib.h>

// NOTE: Reference ->
// https://github.com/SpongePowered/Schematic-Specification/blob/master/versions/schematic-3.md

// Sets up a generic nbt_var with sponge specific headers
static struct nbt_var *sponge_generateRoot() {
  struct nbt_var *root = cnbt_make_compound("Schematic");

  // Metadata
  struct nbt_var *version = cnbt_make_integer(3, "Version");
  cnbt_compound_add(root, version);
  struct nbt_var *dataVersion = cnbt_make_integer(1343, "DataVersion");
  cnbt_compound_add(root, dataVersion);

  // Schematic metadata
  struct nbt_var *metadata = cnbt_make_compound("Metadata");
  struct nbt_var *name = cnbt_make_string("", "Name");
  cnbt_compound_add(metadata, name);
  struct nbt_var *author = cnbt_make_string("", "Author");
  cnbt_compound_add(metadata, author);
  struct nbt_var *date = cnbt_make_long(0, "Date");
  cnbt_compound_add(metadata, date);
  struct nbt_var *requiredMods = cnbt_make_list(NBT_TSTRING, "RequiredMods");
  cnbt_compound_add(metadata, requiredMods);
  cnbt_compound_add(root, metadata);

  // Dimensions
  struct nbt_var *width = cnbt_make_short(0, "Width");
  cnbt_compound_add(root, width);
  struct nbt_var *height = cnbt_make_short(0, "Height");
  cnbt_compound_add(root, height);
  struct nbt_var *length = cnbt_make_short(0, "Length");
  cnbt_compound_add(root, length);

  // Offset
  // Not needed since it defaults to [0, 0, 0]

  return root;
}

struct nbt_var *_globalPalette2NBT() {
  struct nbt_var *palette = cnbt_make_compound("Palette");

  for (size_t i = 0; i < MC_PALETTESIZE; i++) {
    struct nbt_var *entry = cnbt_make_integer(i, mc_palette_get(i));
    cnbt_compound_add(palette, entry);
  }

  return palette;
}

// TODO: implement a varint for the Data array
//  idk why they did not use a integer for this
// HACK: as long as MC_PALETTESIZE does not exceed 127 entries we can just use a
// byte
struct nbt_var *_structure2Sponge(struct MinecraftStructure *structure) {

  struct nbt_var *root = sponge_generateRoot();

  struct nbt_var *blockContainer = cnbt_make_compound("Blocks");
  cnbt_compound_add(blockContainer, _globalPalette2NBT());

  struct nbt_var *data = cnbt_make_byteArray(
      structure->blocks, structure->len_x * structure->len_y * structure->len_z,
      "Data");
  cnbt_compound_add(blockContainer, data);

  cnbt_compound_add(root, blockContainer);
  return root;
}

int libSponge_WriteFile(char const *path,
                        struct MinecraftStructure *structure) {

  cnbt_serialize_nbt(_structure2Sponge(structure), path);
  return 0;
}
