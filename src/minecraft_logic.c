#include "minecraft_logic.h"

#include <stddef.h>
#include <stdlib.h>

// NOTE: Don't forget to update MC_PALETTSIZE when resizing this
char const *mc_palette_get(size_t id) {
  switch (id) {
  case 0:
    return "minecraft:air";
  case 1:
    return "minecraft:redstone_wire";
  case 2:
    return "minecraft:redstone_torch";
  case 3:
    return "minecraft:redstone_wall_torch[facing=north]";
  case 4:
    return "minecraft:redstone_wall_torch[facing=east]";
  case 5:
    return "minecraft:redstone_wall_torch[facing=south]";
  case 6:
    return "minecraft:redstone_wall_torch[facing=west]";
  case 7:
    return "minecraft:stone";
  default:
    return 0;
  }
}

struct MinecraftStructure *mc_make_structure(size_t len_x, size_t len_y,
                                             size_t len_z) {
  struct MinecraftStructure *ret = malloc(sizeof(struct MinecraftStructure));
  ret->len_x = len_x;
  ret->len_y = len_y;
  ret->len_z = len_z;
  ret->blocks = calloc(len_x * len_y * len_z, sizeof(size_t));

  return ret;
}

//
// The entries are indexed by x + z * Width + y * Width * Length
//
size_t mc_structure_index(struct MinecraftStructure *structure, size_t x,
                          size_t y, size_t z) {
  return x + z * structure->len_x + y * structure->len_x * structure->len_z;
}
