#include <stdio.h>

#include "minecraft_logic.h"
#include "minecraft_structures.h"
#include "similar_file.h"
#include "similar_file_format.h"

#include "sponge_file.h"
#include "translation_unit.h"

int main(int argc, char *argv[]) {
  printf("Starting...\n");
  struct SimilarFile *sf = NULL;
  libSimilar_ReadFile(argv[1], &sf);
  printf("Done reading %s\n", argv[1]);

  // Debug print
  libSimilar_PrintComponents(sf);

  // Get the generated minecraft structure

  // Turn it into a sponge file
  struct MinecraftStructure *structure = mc_gate_AND();

  // Write it to disk
  libSponge_WriteFile("/tmp/rawNBTData.nbt", structure);

  return 0;
}
