#include "minecraft_logic.h"
#include "similar_file.h"
#include "similar_file_format.h"
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

// Helper function that offsets positions of all elements so that the upper left
// one is x:0,y:0
static void _SimilarCoordinateTrim(struct SimilarFile *const sf) {
  struct SimilarCoordinate smallest = {INT_MAX, INT_MAX};

  // Get the position of the most upper left thing
  TLNode *tmpComponent = sf->components;
  while (tmpComponent != NULL) {
    struct SimilarComponent *c = tmpComponent->content;
    if (smallest.x > c->position.x) {
      smallest.x = c->position.x;
    }
    if (smallest.y > c->position.y) {
      smallest.y = c->position.y;
    }

    tmpComponent = tmpComponent->next;
  }

  // Subtract this position from all the components
  tmpComponent = sf->components;
  while (tmpComponent != NULL) {
    struct SimilarComponent *c = tmpComponent->content;
    c->position.x -= smallest.x;
    c->position.y -= smallest.y;

    if (c->position.x == 0 && c->position.y == 0) {
      printf("Found the most upper left thing!\n");
    }

    tmpComponent = tmpComponent->next;
  }
}

struct MinecraftStructure *
SimilarFile2MinecraftStructure(struct SimilarFile *const sf) {
  if (sf == NULL)
    return NULL;

  struct MinecraftStructure *ret = malloc(sizeof(struct MinecraftStructure));

  // Trim to 0,0
  _SimilarCoordinateTrim(sf);

  return ret;
}
