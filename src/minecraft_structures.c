#include "minecraft_structures.h"
#include "minecraft_logic.h"

struct MinecraftStructure *mc_gate_AND() {
  struct MinecraftStructure *and = mc_make_structure(6, 2, 3);

  size_t *blocks = and->blocks;
  // Main gate
  // Stone
  blocks[mc_structure_index(and, 2, 0, 0)] = 7;
  blocks[mc_structure_index(and, 2, 0, 1)] = 7;
  blocks[mc_structure_index(and, 2, 0, 2)] = 7;

  // redstone torches
  blocks[mc_structure_index(and, 2, 1, 0)] = 2;
  blocks[mc_structure_index(and, 2, 1, 2)] = 2;

  // redstone wall torches
  blocks[mc_structure_index(and, 3, 0, 1)] = 4;

  // redstone dust
  blocks[mc_structure_index(and, 0, 0, 0)] = 1;
  blocks[mc_structure_index(and, 1, 0, 0)] = 1;
  blocks[mc_structure_index(and, 0, 0, 2)] = 1;
  blocks[mc_structure_index(and, 1, 0, 2)] = 1;
  blocks[mc_structure_index(and, 4, 0, 1)] = 1;
  blocks[mc_structure_index(and, 5, 0, 1)] = 1;

  return and;
}
