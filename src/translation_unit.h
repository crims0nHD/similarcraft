#ifndef TRANSLATION_UNIT_H_
#define TRANSLATION_UNIT_H_

#include "similar_file.h"

struct MinecraftStructure *
SimilarFile2MinecraftStructure(struct SimilarFile *const sf);

#endif // TRANSLATION_UNIT_H_
