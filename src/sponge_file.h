#ifndef SPONGE_FILE_H_
#define SPONGE_FILE_H_

#include "minecraft_logic.h"
#include "minecraft_structures.h"
#include <stddef.h>

int libSponge_WriteFile(char const *path, struct MinecraftStructure *structure);

#endif // SPONGE_FILE_H_
