#ifndef MINECRAFT_LOGIC_H_
#define MINECRAFT_LOGIC_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define MC_PALETTESIZE 8
char const *mc_palette_get(size_t id);

// Struct that holds a MinecraftStructure. This will later be processed by
// sponge_file.
struct MinecraftStructure {
  size_t len_x;
  size_t len_y;
  size_t len_z;

  uint8_t *blocks; // Entries in the pallete
};

size_t mc_structure_index(struct MinecraftStructure *structure, size_t x,
                          size_t y, size_t z);

struct MinecraftStructure *mc_make_structure(size_t len_x, size_t len_y,
                                             size_t len_z);

#endif // MINECRAFT_LOGIC_H_
