#!/usr/bin/env sh

# Convenient build script for similarcraft

# Libraries
cd lib

build_cmake(){
cd $1
mkdir build
cd build
cmake ..
make
cd .. # cnbt
cd .. # lib
}

# cnbt
build_cmake libcnbt

# similar
build_cmake libsimilar

cd ..

# Similarcraft
mkdir build
cd build
cmake ..
make
