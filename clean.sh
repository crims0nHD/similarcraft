#!/usr/bin/env sh

# Script for cleaning working dirs

# Libraries
rm -r ./lib/libsimilar/build
rm -r ./lib/libcnbt/build

# similarcraft
rm -r ./build
